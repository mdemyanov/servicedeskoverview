window.addEventListener("impress:stepenter", function (event) {
    console.log("HASH CHANGE", location.hash);
    switch (location.hash) {
        //case "#/BP_LIST": $(event.target.children[0]).animo({animation: 'bounceIn', duration: 1}); break;
    }
});
document.addEventListener('impress:substepactive', function(event){
    var curSubStep = event.target.id;
    var curSubParent = event.target.parentNode.id;
    console.log('CURRENT STEP PARENT', curSubParent);
    console.log('CURRENT STEP', curSubStep);
    /*Создание анимаций при от объекта события*/
    switch (curSubStep) {
        case ('ITDOWN') : $('#ITDOWN').animo({animation: 'bounceInDown', duration: 1}); break;
        case ('BUSINESSUP') : $('#ITDOWN').animo({animation: 'hinge', duration: 4, keep: true});
            $('#BUSINESSUP').animo({animation: 'rollIn', duration: 1}); break;
    }
    /*Создание анимаций при от родителя объекта события*/
    switch (curSubParent) {
        case ('PROCESS_TEMPLATES') : $(event.target).animo({animation: 'tada', duration: 1}); break;
        case ('WORK_CONTROL') : $(event.target).animo({animation: 'bounceIn', duration: 1}); break;
        case ('WORK') : $(event.target).animo({animation: 'bounceIn', duration: 1}); break;
        case ('EMPLOYEE_CONTROL') : $(event.target).animo({animation: 'bounceIn', duration: 1}); break;
        case ('FINANCE_BENEFITS') : $(event.target).animo({animation: 'tada', duration: 1}); break;
        case ('QM') : $(event.target).animo({animation: 'bounceIn', duration: 1}); break;
    }
});
$('.flipcard').on('click', function() {
    $(this).toggleClass('flip');
});


